package org.tomohavvk.walker.http.endpoints.metas

import org.tomohavvk.walker.protocol.Types.XAuthDeviceId

case class EmptyBodyCommandMeta(authenticatedDeviceId: XAuthDeviceId)
